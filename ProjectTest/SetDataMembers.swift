//
//  SetDataMembers.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

extension MainPageViewController {
    
    func setDataMembers(caller: String, dbController: databaseController?) {
        
        /////////////////////////////////
        //      set data for members
        /////////////////////////////////
        
        if let temp = PublicStruct.PersistentData.array(forKey: PublicStruct.MemberStaticDataKey) as? [Int] {
            PublicStruct.OldMemberStaticData = temp
        }
        
        PublicStruct.NewMemberStaticData = PublicStruct.OldMemberStaticData //get previous nsuserdefault data
        
        /////////////////////////////////
        //      indexer and value filler
        /////////////////////////////////
        if caller != "skipLogin" {
            PublicStruct.OldMemberDataIndex = PublicStruct.PersistentData.integer(forKey: PublicStruct.MemberDataIndexKey)
            PublicStruct.NewMemberDataIndex = PublicStruct.OldMemberDataIndex
            PublicStruct.NewMemberDataIndex = PublicStruct.NewMemberDataIndex + 1
            PublicStruct.NewMemberStaticData.append(PublicStruct.NewMemberDataIndex)
            
            for i in 0...(PublicStruct.NewMemberDataIndex-1){
                PublicStruct.NewMemberStaticData[i] = (i+1)
                
                if i == PublicStruct.NewMemberDataIndex-1 {
                    dbController?.updateMemberNumber(name: String(i + 1))
                }
                
            }
            
            PublicStruct.PersistentData.set(PublicStruct.NewMemberStaticData, forKey: PublicStruct.MemberStaticDataKey) // set the index before index is updated
            PublicStruct.PersistentData.set(PublicStruct.NewMemberDataIndex, forKey: PublicStruct.MemberDataIndexKey)
        }
        /////////////////////////////////
        //      indexer and value filler end
        /////////////////////////////////
        
        PublicStruct.NewMemberData = PublicStruct.NewMemberStaticData.map(
            {
                (number: Int) -> String in
                return String(number)
            }
        )
        TableSectionStruct.data[0]=PublicStruct.NewMemberData
    }
}
