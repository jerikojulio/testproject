//
//  TimerController.swift
//  ProjectTest
//
//  Created by Jeriko on 1/12/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation

class TimerController {
    
    var controllerTimerModel = TimerModel()
    
    func getModel(timerModel: TimerModel){
        controllerTimerModel = timerModel
    }
    
    func getCount()->Int{
        
        controllerTimerModel.count = controllerTimerModel.count + 1
        controllerTimerModel.circleTime = 1.5
        
        return controllerTimerModel.count
    }
    
    func getCount2()->Int{
        if controllerTimerModel.count >= 2 {
            controllerTimerModel.count2 = controllerTimerModel.count2 + 1
        }
        return controllerTimerModel.count2
    }
    
    func updateCircleModel (){
        controllerTimerModel.circleTime = controllerTimerModel.circleTime + 0.05
        if controllerTimerModel.circleTime >= 2.0 {
            controllerTimerModel.circleTime = 0.0
        }
    }
    
    func getCircleTime()->Double{
        return controllerTimerModel.circleTime
    }
    
    
}
