//
//  ChildCell.swift
//  ProjectTest
//
//  Created by Jeriko on 12/7/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class ChildCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var labelChild: UILabel!
}
