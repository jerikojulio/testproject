//
//  CircleTwo.swift
//  ProjectTest
//
//  Created by Jeriko on 1/13/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class CircleTwo: UIView {
    var circlePath = UIBezierPath()
    var endAngle = 1.4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func defineCircle(){
        let circleCenter = CGPoint(x: self.frame.size.height/2, y: self.frame.size.width/2)
        let circleRadius = CGFloat(self.frame.size.width/3)
        circlePath = UIBezierPath(arcCenter: circleCenter,
                                  radius: circleRadius,
                                  startAngle: CGFloat(M_PI * 1.5),
                                  endAngle: CGFloat(M_PI * endAngle),
                                  clockwise: true)
        circlePath.lineWidth = 2.0
    }
    
    func updateCircleView (){
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect){
        circlePath.stroke()
    }
}
