//
//  FirebaseTable.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import FirebaseDatabase

extension TimerDate {
    
    
    @IBAction func openSearchPage(_ sender: UIButton) {
        
        let alert = UIAlertController(
            title: "New Name",
            message: "Add a new name",
            preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default,
                                       handler: { (action:UIAlertAction) -> Void in
                                        
                                        let textField = alert.textFields!.first
                                        print(textField?.text ?? "notext")
                                        let newName = FirebaseDataModel(name: (textField?.text)!)
                                        self.items.append(newName)
                                        self.firebaseTable.reloadData()
                                        
                                        let itemRef = self.ref.child((textField?.text?.lowercased())!)
                                        itemRef.setValue(newName.toAnyObject())
//                                        itemRef.setValue(self.toAnyObject(name: (textField?.text)!))
        })
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default) { (action: UIAlertAction) -> Void in
        }
        
        alert.addTextField {
            (textField: UITextField) -> Void in
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert,
                animated: true,
                completion: nil)
        print(ref)
        
        //        self.navigationController?.pushViewController(SearchBarController(), animated: true)
    }
    
    @IBAction func getData(_ sender: UIButton) {
        ref.observe(.value, with: { snapshot in
//            print(snapshot.value)
            // 2
            var newItems: [FirebaseDataModel] = []
            
            // 3
            for item in snapshot.children {
                // 4
                let fetchedValues = FirebaseDataModel(snapshot: item as! FIRDataSnapshot)
                newItems.append(fetchedValues)
            }
            
            // 5
            self.items = newItems
            self.firebaseTable.reloadData()
            
        })
    }
    
}
