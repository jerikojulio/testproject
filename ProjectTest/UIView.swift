//
//  UIView.swift
//  ProjectTest
//
//  Created by Jeriko on 1/17/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

extension UIView {
    // Remove the given characters in the range
    public func backgroundGradient()
    {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [
            Helper.gradientTopColor(),
            Helper.gradientBottomColor()]
        self.layer.insertSublayer(gradient, at: 0)
    }
}
