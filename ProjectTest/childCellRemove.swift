//
//  SectionCollapse.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

extension TableSectionViewController {
    
    func childCellRemove(indexPath: IndexPath, tableView: UITableView){
        
        var newIndexpath = indexPath
        newIndexpath.row = newIndexpath.row + 1
        
        tableView.beginUpdates()
        
        //        for i in (newIndexpath.row...newIndexpath.row + 3).reversed() {
        for i in (newIndexpath.row...newIndexpath.row + CellType.ChildCellTypeCount.rawValue - 1).reversed() {
            let currentIndex = NSIndexPath(row: i, section: newIndexpath.section)
            tableView.deleteRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.bottom)
            TableSectionStruct.data[newIndexpath.section].remove(at: i)
            TableSectionStruct.currentCellType2.remove(at: i)
        }
        
        tableView.endUpdates()
    }
}
