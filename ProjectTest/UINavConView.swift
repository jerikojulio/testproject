//
//  UINavConViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 12/6/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class UINavConViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension UINavConViewController: UINavigationControllerDelegate{
    
    /////////////////////////////////
    //custom transition
    /////////////////////////////////
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CustomTransitionAnimator()
    }
}
