//
//  UIViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 1/17/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

extension UIViewController {
    // Remove the given characters in the range
    public func backgroundGradient()
    {
        ForSubLayers.sublayer = CAGradientLayer()
        ForSubLayers.sublayer.frame = self.view.bounds
        ForSubLayers.sublayer.colors = [
            Helper.gradientTopColor(),
            Helper.gradientBottomColor()]
        
        self.view.layer.insertSublayer(ForSubLayers.sublayer, at: 0)
    }
    
    public func updateBackgroundGradientSize(viewHeight: CGFloat, viewWidth: CGFloat){
        let newSize = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        ForSubLayers.sublayer.frame = newSize
        ForSubLayers.sublayer.layoutSublayers()
    }
}

extension UIViewController {
    func presentController (callerView: UIViewController, toBeCalled: UIViewController){
        let VC = toBeCalled
        callerView.navigationController?.pushViewController(VC, animated: true)
    }
}
