//
//  TableDataSource.swift
//  ProjectTest
//
//  Created by Jeriko on 12/22/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class TableViewDataSource: NSObject, UITableViewDataSource {
    
    var tempFirebaseTableData02 = [ContactTableModel]()
    
    func setDatabase(Data: [ContactTableModel]){
        tempFirebaseTableData02 = Data
    }
    
    /////////////////////////////////
    //sections
    /////////////////////////////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableSectionStruct.SectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return TableSectionStruct.SectionTitle[section]
    }
    
    
    //////////////////////////////////
    //rows
    //////////////////////////////////
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if section == 1 {
            count = tempFirebaseTableData02.count
        } else {
            count = TableSectionStruct.data[section].count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
        case 0:
            switch TableSectionStruct.currentCellType2[indexPath.row]{
            case .nameCell:
                let cellChild : ChildCellTwo = tableView.dequeueReusableCell(withIdentifier: "childcelltwo", for: indexPath) as! ChildCellTwo
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                let tempString = PublicStruct.NewEmailStaticData[indexPath.row-(indexForChild)]
                cellChild.childCelltTwo.text = ViewControllerTable.getName(filteredName: tempString)
                return cellChild
            case .dateCell:
                let cellChild : ChildCell = tableView.dequeueReusableCell(withIdentifier: "child", for: indexPath) as! ChildCell
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                cellChild.labelChild.text = PublicStruct.NewDate[indexPath.row-(indexForChild)]
                return cellChild
            case .emailCell:
                let cellChild : EmailCell = tableView.dequeueReusableCell(withIdentifier: "emailcell", for: indexPath) as! EmailCell
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                cellChild.textLabel?.text = ViewControllerMainPage.EmailData[indexPath.row-(indexForChild)]
                cellChild.textLabel?.textColor = UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 1)
                return cellChild
            case .address:
                let cellChild : EmailCell = tableView.dequeueReusableCell(withIdentifier: "emailcell", for: indexPath) as! EmailCell
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                cellChild.textLabel?.text = MapStruct.homesName[indexPath.row-(indexForChild)]
                return cellChild
            case .phoneNumber:
                let cellChild : EmailCell = tableView.dequeueReusableCell(withIdentifier: "emailcell", for: indexPath) as! EmailCell
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                cellChild.textLabel?.text = TableSectionStruct.phoneNumber[indexPath.row-(indexForChild)]
                cellChild.textLabel?.textColor = UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 1)
                return cellChild
            default:
                let cell : TableSectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableSectionViewCell
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                let tempString = PublicStruct.NewEmailStaticData[indexPath.row-(indexForChild)]
                cell.memberLabel.text = "Member \(PublicStruct.NewMemberData[indexPath.row-(indexForChild)]) : \(ViewControllerTable.getName(filteredName: tempString))"
                cell.arrowImage.image = TableSectionStruct.currentArrow[indexPath.row-(indexForChild)]
                cell.backgroundColor = UIColor(red: 234/255, green: 250/255, blue: 255/255, alpha: 1)
                return cell
            }
        case 1:
            switch tempFirebaseTableData02[indexPath.row].cellState{
            case .opened:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = "Member \(tempData.memberNumber)"
                cell.backgroundColor = UIColor(red: 201/255, green: 252/255, blue: 255/255, alpha: 1)
                return cell
            case .closed:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = "Member \(tempData.memberNumber)"
                cell.backgroundColor = UIColor(red: 201/255, green: 252/255, blue: 255/255, alpha: 1)
                return cell
            case .nameCell:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = tempData.name
                cell.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                return cell
            case .dateCell:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = tempData.regDate
                cell.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                return cell
            case .address:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = tempData.address
                cell.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                return cell
            case .phoneNumber:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                let tempData = tempFirebaseTableData02[indexPath.row]
                cell.textLabel?.text = tempData.phoneNumber
                cell.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                return cell
            default:
                let cell : FirebaseContact = tableView.dequeueReusableCell(withIdentifier: "firebase", for: indexPath) as! FirebaseContact
                cell.textLabel?.text = "ERROR"
                cell.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                return cell
            }
            
        case 2:
            let cell : TableSectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableSectionViewCell
            cell.memberLabel.text = TableSectionStruct.data[indexPath.section][indexPath.row]
            cell.arrowImage.image = nil
            cell.backgroundColor = UIColor(red: 116/255, green: 226/255, blue: 232/255, alpha: 1)
            
            return cell
        case 3:
            let cell : CollectionContainerCell = tableView.dequeueReusableCell(withIdentifier: "collection", for: indexPath) as! CollectionContainerCell
            return cell
        default:
            let cell : TableSectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableSectionViewCell
            cell.memberLabel.text = TableSectionStruct.data[indexPath.section][indexPath.row]
            cell.arrowImage.image = nil
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        /*
         if TableSectionStruct.currentCellType2[indexPath.row] == .closed && indexPath.section == 0{
         if editingStyle == UITableViewCellEditingStyle.delete{
         
         TableSectionStruct.currentCellType2.remove(at: indexPath.row)
         PublicStruct.NewEmailStaticData.remove(at: indexPath.row)
         TableSectionStruct.data[0].remove(at: indexPath.row)
         TableSectionStruct.data[1].remove(at: indexPath.row)
         TableSectionStruct.data[2].remove(at: indexPath.row)
         tableView.reloadData()
         
         /////////////////////////////////
         //set data for members
         /////////////////////////////////
         PublicStruct.NewMemberDataIndex = PublicStruct.PersistentData.integer(forKey: PublicStruct.MemberDataIndexKey)
         
         PublicStruct.NewMemberDataIndex = PublicStruct.NewMemberDataIndex - 1
         
         PublicStruct.PersistentData.set(PublicStruct.NewMemberDataIndex, forKey: PublicStruct.MemberDataIndexKey)
         
         for i in 0...(PublicStruct.NewMemberDataIndex-1){
         TableSectionStruct.data[0][i] = String(i+1)
         }
         
         PublicStruct.NewMemberStaticData = TableSectionStruct.data[0].map{Int($0)!}
         
         PublicStruct.PersistentData.set(PublicStruct.NewMemberStaticData, forKey: PublicStruct.MemberStaticDataKey) // set the index before index is updated
         
         
         /////////////////////////////////
         //set data for email sections
         /////////////////////////////////
         PublicStruct.PersistentData.set(TableSectionStruct.data[1], forKey: PublicStruct.EmailStaticDataKey)
         
         /////////////////////////////////
         //set data for phone sections
         /////////////////////////////////
         PublicStruct.PersistentData.set(TableSectionStruct.data[2], forKey: PublicStruct.PhoneStaticDataKey)
         
         /////////////////////////////////
         //set data for registration date
         /////////////////////////////////
         PublicStruct.NewDate.remove(at: indexPath.row)
         
         PublicStruct.PersistentData.set(PublicStruct.NewDate, forKey: PublicStruct.DateKey)
         /////////////////////////////////
         //set data end
         /////////////////////////////////
         }
         }
         */
        if tempFirebaseTableData02[indexPath.row].cellState == .closed && indexPath.section == 1 && editingStyle == .delete{
            let trash = tempFirebaseTableData02[indexPath.row]
            trash.ref?.removeValue()
            // delete the table view row
//            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        }
        
    }
}

