//
//  ContactTableModel.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
import Firebase

struct ContactTableModel {
    var memberNumber: String
    var name: String
    var regDate: String
    var email: String
    var address: String
    var phoneNumber: String
    var cellState = CellType.closed
    
    let ref: FIRDatabaseReference?
    
    init(memberNumber: String, name: String, regDate: String, email: String, address: String, phoneNumber: String) {
        self.memberNumber = memberNumber
        self.name = name
        self.regDate = regDate
        self.email = email
        self.address = address
        self.phoneNumber = phoneNumber
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        memberNumber = snapshotValue["memberNumber"] as! String
        name = snapshotValue["name"] as! String
        regDate = snapshotValue["regDate"] as! String
        email = snapshotValue["email"] as! String
        address = snapshotValue["address"] as! String
        phoneNumber = snapshotValue["phoneNumber"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "memberNumber": memberNumber,
            "name": name,
            "regDate": regDate,
            "email": email,
            "address": address,
            "phoneNumber": phoneNumber,
        ]
    }
    
}
