//
//  SecondViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import MapKit

class MapView: UIViewController{
    
    let regionRadius: CLLocationDistance = 20000
    var initialLocation = CLLocation(latitude: -6.21462, longitude: 106.84513)
    let initialLocation2D = CLLocationCoordinate2D(latitude: -6.21462, longitude: 106.84513)
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name(rawValue: "centermapupdate"), object: nil)
        reload()
    }
    
    func reload(){
        if initialLocation != MapStruct.mapCenter {
            initialLocation = MapStruct.mapCenter
        }
        
        if self.mapView.annotations.count != 0 {
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        
        self.title = "Position"
        centerMapOnLocation(location: initialLocation)
        
        if MapStruct.userHomes.count == 0 {
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        
        //        mapView.annotations.removeAll()
        
        for i in 0...MapStruct.userHomes.count-1{
            let annotation = MKPointAnnotation()
            annotation.coordinate = MapStruct.userHomes[i]
            annotation.title = ViewControllerTable.getName(filteredName: ViewControllerMainPage.EmailData[i])
            mapView.addAnnotation(annotation)
        }
        
        //        if mapView.annotations.count != 0 {
        //            mapView.selectAnnotation(mapView.annotations[MapStruct.indexPath], animated: true)
        //        }
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
