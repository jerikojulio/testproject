//
//  RandomLines.swift
//  ProjectTest
//
//  Created by Jeriko on 1/13/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class RandomLines: UIView {
    var lines = UIBezierPath()
    var time = Float()
    var calc = MapStruct()
    
    var lastPoint = CGPoint()
    var randomPoint = CGPoint()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func defineRandomPoint(){
        
        if time > 0.25 {
            lastPoint = randomPoint
        } else {
            lastPoint = CGPoint(
                x: calc.randomCGFloat(min: 0, max: self.bounds.width),
                y: calc.randomCGFloat(min: 0, max: self.bounds.height))
        }
        
        
        randomPoint = CGPoint(
            x: calc.randomCGFloat(min: 0, max: self.bounds.width),
            y: calc.randomCGFloat(min: 0, max: self.bounds.height))
        
        lines.move(to: lastPoint)
        
        lines.addLine(to: randomPoint)
        
        lines.lineWidth = 0.5
    }
    
    func updateLines(){
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect){
        lines.stroke()
    }
}
