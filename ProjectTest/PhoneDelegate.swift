//
//  PhoneDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 12/27/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit

class PhoneFieldDelegate: NSObject, UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
        if PublicStruct.phoneStr.characters.count < PublicStruct.phoneStrMaxCount
        {
            if Int(string) != nil {
                ViewControllerMainPage.phoneFieldCheck(fieldName: textField.text!, input: string)
                buttonEnabler()
                return true
            }
            
            if string == "" {
                ViewControllerMainPage.phoneFieldCheck(fieldName: textField.text!, input: string)
                buttonEnabler()
                return true
            }
            return false
        }
        
        if PublicStruct.phoneStr.characters.count == PublicStruct.phoneStrMaxCount && string == "" {
            ViewControllerMainPage.phoneFieldCheck(fieldName: textField.text!, input: string)
            buttonEnabler()
            return true
        }
        return false
    }
}
