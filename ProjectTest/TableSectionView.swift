//
//  TableSectionViewController.swift
//  ProjectTest
//
//  Created by Jeriko on 12/1/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import Firebase

class TableSectionViewController: UIViewController {
    
    let ref = FIRDatabase.database().reference(withPath: "projecttest-16bdd")
    var items: [ContactTableModel] = []
    let itemsEmpty: [ContactTableModel] = []
    
    @IBOutlet weak var tableSectionView: UITableView!
    
    let datasource = TableViewDataSource()
    //    let delegate = TableViewDelegate()
    
    var editTitle = "Edit"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableSectionView.delegate = self
        //        tableSectionView.delegate = delegate
        tableSectionView.dataSource = datasource
        
        ViewControllerTable.registerNib(nibName: "ChildCell", forCellReuseIdentifier: "child", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "TableSectionViewCell", forCellReuseIdentifier: "cell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "EmailCell", forCellReuseIdentifier: "emailcell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "ChildCellTwo", forCellReuseIdentifier: "childcelltwo", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "CollectionContainerCell", forCellReuseIdentifier: "collection", table: tableSectionView)
        
        ViewControllerTable.registerNib(nibName: "TableViewSection", forCellReuseIdentifier: "sectionCell", table: tableSectionView)
        ViewControllerTable.registerNib(nibName: "SecondSectionView", forCellReuseIdentifier: "sectioncell", table: tableSectionView)
        
        ViewControllerTable.registerNib(nibName: "FirebaseContact", forCellReuseIdentifier: "firebase", table: tableSectionView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateSecondSection), name: NSNotification.Name(rawValue: "maplocationupdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toast), name: NSNotification.Name(rawValue: "contactupdated"), object: nil)
        
        if (TableSectionStruct.data[0].count-1) >= 0 {
            for i in 0...(TableSectionStruct.data[0].count-1) {
                TableSectionStruct.currentCellType2.append(.closed)
                if TableSectionStruct.currentArrow.count != TableSectionStruct.data[0].count{
                    TableSectionStruct.currentArrow.append(#imageLiteral(resourceName: "arrowRight.png"))
                }
                TableSectionStruct.currentArrow[i] = #imageLiteral(resourceName: "arrowRight")
            }
        }
        
        self.title = "Data"
        
        TempData.tempArray = TableSectionStruct.dummyDataCell
        
        //Get all views in the xib
        let allViewsInXibArray = Bundle.main.loadNibNamed("TableViewBG", owner: TableViewBG(), options: nil)
        
        //If you only have one view in the xib and you set it's class to MyView class
        let myView = allViewsInXibArray?.first as! TableViewBG
        
        if let bounds = self.navigationController?.view.bounds{
            myView.frame = bounds
        }
        
        //        if let frame = self.navigationController?.view.frame{
        //            myView.frame = frame
        //        }
        
        myView.backgroundGradient()
        
        tableSectionView.backgroundView = myView
        
        TableSectionStruct.mainTableView = tableSectionView
        
        ref.queryOrdered(byChild: "memberNumber").observe(.value, with: {
            snapshot in
            var newItems: [ContactTableModel] = []
            
            // 3
            for item in snapshot.children {
                // 4
                var fetchedValues = ContactTableModel(snapshot: item as! FIRDataSnapshot)
                fetchedValues.email = FirebaseHelper.emailConversion(filteredName: fetchedValues.email)
                newItems.append(fetchedValues)
            }
            
            // 5
            self.items = newItems
            TableSectionStruct.tempFirebaseTableData = newItems
            self.datasource.setDatabase(Data: self.items)
            self.tableSectionView.reloadData()
        })
        //debugging
        
        
        let menu_button_ = UIBarButtonItem(title: editTitle,
                                           style: UIBarButtonItemStyle.plain ,
                                           target: self, action: #selector(tableEditingStyle))
        
        self.tabBarController?.navigationItem.rightBarButtonItem = menu_button_
        //        self.navigationController?.navigationItem.rightBarButtonItem = menu_button_
        //        self.navigationItem.rightBarButtonItem = menu_button_
        
        let backButton = UIBarButtonItem(title: "Back",
                                         style: UIBarButtonItemStyle.plain ,
                                         target: self, action: #selector(popCurrentView))
        self.tabBarController?.navigationItem.leftBarButtonItem = backButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func tableEditingStyle(){
        tableSectionView.isEditing = !tableSectionView.isEditing
        if tableSectionView.isEditing {
            editTitle = "Done"
            self.tabBarController?.navigationItem.rightBarButtonItem?.title = editTitle
        } else {
            editTitle = "Edit"
            self.tabBarController?.navigationItem.rightBarButtonItem?.title = editTitle
        }
    }
    
    func popCurrentView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        if (TableSectionStruct.data[0].count-1) >= 0 {
            for i in (0..<TableSectionStruct.data[0].count).reversed() {
                if TableSectionStruct.data[0][i] == "test" {
                    TableSectionStruct.data[0].remove(at: i)
                }
            }
            
            for i in (0..<TableSectionStruct.currentCellType2.count).reversed() {
                switch TableSectionStruct.currentCellType2[i]{
                case .opened:
                    TableSectionStruct.currentCellType2[i] = .closed // set opened state to closed state
                default:
                    TableSectionStruct.currentCellType2.remove(at: i)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func updateSecondSection (){
        TableSectionStruct.data[1] = MapStruct.homesName
        tableSectionView.reloadData()
        
    }
    
    
    func toast (){
        let rect = CGRect(x: (self.view.frame.size.width - self.view.frame.size.width*0.4)/2,
                          y: self.view.frame.size.width*1.4,
                          width: self.view.frame.size.width*0.35,
                          height: self.view.frame.size.height*0.07)
        let toastLabel = UILabel(frame: rect)
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.blue
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        
        if TableSectionStruct.contactStatus == true {
            toastLabel.text = "Contact Exist!"
        } else {
            toastLabel.text = "Contact Added"
        }
        
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(
            withDuration: 0.3,
            delay: 1.3,
            options: .curveEaseOut,
            animations: {
                toastLabel.alpha = 0.0
        }, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
