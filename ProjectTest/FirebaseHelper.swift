//
//  FirebaseHelper.swift
//  ProjectTest
//
//  Created by Jeriko on 1/19/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation

class FirebaseHelper {
    static func emailConversion (filteredName: String) -> String {
        
//        let delimiter = "@"
//        var token = filteredName.components(separatedBy: delimiter)
//        let tempString02 = token[0].replacingOccurrences(of: "_", with: " ")
//        let newName = tempString02.replacingOccurrences(of: ".", with: " ")
        
        let newName = filteredName.replacingOccurrences(of: ".", with: "_dot_")
        
        return newName
    }
}
