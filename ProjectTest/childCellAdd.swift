//
//  childCellAdd.swift
//  ProjectTest
//
//  Created by Jeriko on 1/13/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

extension TableSectionViewController {
    
    func childCellAdd(indexPath: IndexPath, tableView: UITableView){
        
        let test = "test"
        var newIndexpath = indexPath
        var currentCellType = 1
        
        tableView.beginUpdates()
        
        //        for i in newIndexpath.row...newIndexpath.row + 3 {
        for i in newIndexpath.row...newIndexpath.row + CellType.ChildCellTypeCount.rawValue - 1 {
            //            let currentIndex = NSIndexPath(row: i, section: newIndexpath.section)
            newIndexpath.row = newIndexpath.row + 1
            tableView.insertRows(at: [newIndexpath], with: UITableViewRowAnimation.top)
            
            TableSectionStruct.data[indexPath.section].insert(test, at: i)
            
            switch currentCellType{
            case 1:
                TableSectionStruct.currentCellType2.insert(CellType.nameCell, at: newIndexpath.row) // insert child row, code = 2
            case 2:
                TableSectionStruct.currentCellType2.insert(CellType.dateCell, at: newIndexpath.row) // insert child row, code = 3
            case 3:
                TableSectionStruct.currentCellType2.insert(CellType.emailCell, at: newIndexpath.row) // insert child row, code = 4
            case 4:
                TableSectionStruct.currentCellType2.insert(CellType.address, at: newIndexpath.row) // insert child row, code = 4
            case 5:
                TableSectionStruct.currentCellType2.insert(CellType.phoneNumber, at: newIndexpath.row) // insert child row, code = 4
            default:
                break
            }
            currentCellType = currentCellType + 1
        }
        
        tableView.endUpdates()
        
    }
}
