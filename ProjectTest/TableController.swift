//
//  Helper.swift
//  ProjectTest
//
//  Created by Jeriko on 12/9/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import Foundation


class ViewControllerTable {
    static func registerNib(nibName: String, forCellReuseIdentifier: String, table: UITableView){
        let nib = UINib(nibName: nibName, bundle: nil)
        table.register(nib, forCellReuseIdentifier: forCellReuseIdentifier)
    }
    
    static func getName (filteredName: String) -> String {
        
        let delimiter = "@"
        var token = filteredName.components(separatedBy: delimiter)
        let tempString02 = token[0].replacingOccurrences(of: "_", with: " ")
        let newName = tempString02.replacingOccurrences(of: ".", with: " ")
        
        return newName
    }
    
    static func childRowChecker (indexrow:Int) -> Int {
        
        var finalIndex = 0
        
        for i in 0...indexrow {
            
            switch TableSectionStruct.currentCellType2[i]{
            case .ChildCellTypeCount: //not child cell
                break
            case .opened: //not child cell
                break
            case .closed: //not child cell
                break
            default:
                finalIndex = finalIndex + 1
            }
        }
        
        return finalIndex
    }
    
}
