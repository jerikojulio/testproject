//
//  TableStruct.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
import UIKit

struct TableSectionStruct {
    //    static var data = [[String]]()
    static var data = [
        ["dummy string"],
        ["dummy string"],
        ["dummy string"],
        ["dummy string"]
    ]
    
    static var dataBackup = [
        ["dummy string"],
        ["dummy string"],
        ["dummy string"],
        ["dummy string"]
    ]
    static var SectionTitle = ["Members","Home","Phone Number","Test"]
    static var dummyDataCell = ["1","2"]
    
    static var currentCellType2 = [CellType]()
    
    static var contactStatus = Bool()
    static var HeaderType = Int()
    static var currentArrow = [UIImage]()
    static var isSectionOpened = [Bool](repeating: true, count: SectionTitle.count)
    static var currentSection = Int()
    static var mainTableView = UITableView()
    static var touchLocation = CGPoint()
    
    static var homeAddress = [String()]
    static var phoneNumber = [String()]
    
    static var tempFirebaseTableData = [ContactTableModel]()
}

enum CellType: Int {
    case nameCell = 0,
    dateCell,
    emailCell,
    address,
    phoneNumber,
    ChildCellTypeCount //used as indexer, place new childCell cases before this case
    case opened
    case closed
}
