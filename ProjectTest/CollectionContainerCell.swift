//
//  CollectionContainerCell.swift
//  ProjectTest
//
//  Created by Jeriko on 1/11/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class CollectionContainerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionScrollTest: UICollectionView!
    
    var items=[String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionScrollTest.dataSource = self
        collectionScrollTest.delegate = self
        
        collectionScrollTest.backgroundColor = UIColor.clear
        
        let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
        collectionScrollTest.register(nib, forCellWithReuseIdentifier: "collectionView")
        
        for i in 0...10{
            items.append(String(i+1))
        }
        
        if let layout = collectionScrollTest.collectionViewLayout as? UICollectionViewFlowLayout {
            //            let itemWidth = view.bounds.width / 3.0
            //            let itemHeight = layout.itemSize.height
            layout.itemSize = CGSize(width: 100, height: 100)
            layout.minimumLineSpacing = 125
            layout.headerReferenceSize.width = 62.5
            layout.footerReferenceSize.width = 62.5
            layout.invalidateLayout()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let cellWidth = collectionScrollTest.bounds.width
        
        let page: CGFloat
        // Calculate the proposed "page"
        let proposedPage = targetContentOffset.pointee.x / cellWidth
        
        // 3.25 should return page 3: floor(3.95) == floor(3)
        // 3.3+ should return page 4: floor(4.0+) != floor(3)
        if floor(proposedPage + 0.7) == floor(proposedPage)
            && scrollView.contentOffset.x <= targetContentOffset.pointee.x {
            page = floor(proposedPage)
        }
        else if floor(proposedPage + 0.7) == floor(proposedPage)
            && scrollView.contentOffset.x >= targetContentOffset.pointee.x {
            page = floor(proposedPage)
        }
        else {
            //            page = floor(proposedPage)
            page = floor(proposedPage + 1)
            //            page = floor(proposedPage - 1)
        }
        
        // Replace the end position of the scroll
        targetContentOffset.pointee = CGPoint(
            //            x: cellWidth * page - ((self.view.frame.width-250)*0.5),
            //            x: cellWidth * page - 62.5,
            x: cellWidth * page,
            y: targetContentOffset.pointee.y
        )
        print(cellWidth*page)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CollectionViewCell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionView", for: indexPath) as! CollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.myLabel.text = self.items[indexPath.item]
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        // Customize cell height
        //        cell.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        
        return cell
    }
    
}
