//
//  AnimateArrow.swift
//  ProjectTest
//
//  Created by Jeriko on 1/5/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
import UIKit

extension TableSectionViewController {
    
    func arrowToDown(cell: TableSectionViewCell, indexPath: Int){
        let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath)
        TableSectionStruct.currentArrow[indexPath-indexForChild] = #imageLiteral(resourceName: "arrowDown.png")
        cell.arrowImage.image = TableSectionStruct.currentArrow[indexPath-indexForChild]
    }
    
    func arrowToRight(cell: TableSectionViewCell, indexPath: Int){
        let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath)
        TableSectionStruct.currentArrow[indexPath-indexForChild] = #imageLiteral(resourceName: "arrowRight.png")
        cell.arrowImage.image = TableSectionStruct.currentArrow[indexPath-indexForChild]
    }
}
