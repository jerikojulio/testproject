//
//  FirebaseUpdate.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

class databaseController {
    
    var tempDatabase = ContactTableModel(memberNumber: "memberNumber",
                                         name: "name",
                                         regDate: "regdate",
                                         email: "email",
                                         address: "address",
                                         phoneNumber: "phoneNumber")
    
    func getModel(database: ContactTableModel){
        tempDatabase = database
    }
    
    func updateMemberNumber(name: String){
        tempDatabase.memberNumber = name
    }
    
    func updateName(name: String){
        tempDatabase.name = name
    }
    
    func updateDate(date: String){
        tempDatabase.regDate = date
    }
    
    func updateAddress(address: String){
        tempDatabase.address = address
        print(address)
    }
    
    func updatePhone(phone: String){
        tempDatabase.phoneNumber = phone
    }
    
    func updateDatabase(database: ContactTableModel, viewCaller: MainPageViewController){
        let itemRef = viewCaller.ref.child(tempDatabase.name)
        itemRef.setValue(tempDatabase.toAnyObject())
    }
}
