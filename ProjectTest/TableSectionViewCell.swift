//
//  TableSectionViewCell.swift
//  ProjectTest
//
//  Created by Jeriko on 12/1/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

class TableSectionViewCell: UITableViewCell {

    @IBOutlet weak var memberLabel: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
