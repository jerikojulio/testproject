//
//  TimerDate.swift
//  ProjectTest
//
//  Created by Jeriko on 1/3/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import FirebaseDatabase

class TimerDate: UIViewController {
    
    let ref = FIRDatabase.database().reference(withPath: "projecttest-16bdd")
    @IBOutlet weak var firebaseTable: UITableView!
    var items: [FirebaseDataModel] = []
    
    var timerModel = TimerModel()
    let timerController = TimerController()
    
    
    @IBOutlet weak var label01: UILabel!
    @IBOutlet weak var label02: UILabel!
    @IBOutlet weak var circleSecond: CircleTwo!
    @IBOutlet weak var lines: RandomLines!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseTable.delegate = self
        firebaseTable.dataSource = self
        ViewControllerTable.registerNib(nibName: "TableSectionViewCell", forCellReuseIdentifier: "cell", table: firebaseTable)
        
        // Do any additional setup after loading the view.
        timerModel.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateModel), userInfo: nil, repeats: true)
        timerModel.timer2 = Timer.scheduledTimer(timeInterval: 0.025, target: self, selector: #selector(updateCircleModel), userInfo: nil, repeats: true)
        timerModel.timer3 = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(updateLineModel), userInfo: nil, repeats: true)
        
        label01.layer.cornerRadius = 0.5 * label01.bounds.size.width
        label01.layer.borderColor = UIColor(red:0.0/255.0, green:0.0/255.0, blue:0.0/255.0, alpha:1).cgColor as CGColor
        label01.layer.borderWidth = 2.0
        label01.clipsToBounds = true
        
        
        timerController.getModel(timerModel: timerModel)
        
        ref.observe(.value, with: { snapshot in
            // 2
            var newItems: [FirebaseDataModel] = []
            
            // 3
            for item in snapshot.children {
                // 4
                let fetchedValues = FirebaseDataModel(snapshot: item as! FIRDataSnapshot)
                newItems.append(fetchedValues)
            }
            
            // 5
            self.items = newItems
            self.firebaseTable.reloadData()
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateModel(){
        label01.text = String(timerController.getCount())
        label02.text = String(timerController.getCount2())
    }
    
    func updateCircleModel(){
        timerController.updateCircleModel()
        circleSecond.defineCircle()
        circleSecond.endAngle = timerController.getCircleTime()
        circleSecond.updateCircleView()
    }
    
    func updateLineModel (){
        lines.time = lines.time + 0.25
        lines.defineRandomPoint()
        lines.updateLines()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
