//
//  FirebaseTableDataSource.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation


extension TimerDate: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TableSectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableSectionViewCell
        
        let currentItem = items[indexPath.row]
        
        cell.memberLabel.text = currentItem.name
        cell.arrowImage.image = nil
        cell.backgroundColor = UIColor.clear
        return cell
    }
}
