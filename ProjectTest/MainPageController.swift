//
//  ViewControllerMainPage.swift
//  ProjectTest
//
//  Created by Jeriko on 12/9/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation

class ViewControllerMainPage {
    
    
    static var EmailData = [String]()
    
    
    static func StrCountCheck()
    {
        if PublicStruct.phoneStrCount >= 10{
            PublicStruct.phoneFieldFilled = true
        } else {
            PublicStruct.phoneFieldFilled = false
        }
        
        if PublicStruct.emailStrCount >= 12
        {
            PublicStruct.emailFieldFilled = true
        } else {
            PublicStruct.emailFieldFilled = false
        }
    }
    
    static func nameFieldCheck(fieldName: String, input: String) -> Void{
        PublicStruct.emailStr = fieldName + input
        PublicStruct.emailStrCount = PublicStruct.emailStr.characters.count
        StrCountCheck()
        
    }
    static func phoneFieldCheck(fieldName: String, input: String) -> Void{
        PublicStruct.phoneStr = fieldName + input
        PublicStruct.phoneStrCount = PublicStruct.phoneStr.characters.count
        StrCountCheck()
    }
    
    static func emailNotExistCheck(string: String)-> Bool{
        if TableSectionStruct.data[2].count > 0{
            for i in 0...(TableSectionStruct.data[2].count - 1){
                if TableSectionStruct.data[2][i] == string{
                    return false
                }
            }
        }
        return true
    }
    
    
    
    static func setDataPhone (appending: String, controller: databaseController?) {
                
        /////////////////////////////////
        //      set data for phone sections
        /////////////////////////////////
        
        PublicStruct.OldPhoneStaticData = PublicStruct.PersistentData.stringArray(forKey: PublicStruct.PhoneStaticDataKey) ?? [String]() //get nsuser data to array
        
        PublicStruct.NewPhoneStaticData = PublicStruct.OldPhoneStaticData //get previous nsuserdefault data
        
        if appending != "nil" {
            PublicStruct.NewPhoneStaticData.append(appending) //append the new data to the old array
            controller?.updatePhone(phone: appending)
        }
        
        PublicStruct.PersistentData.set(PublicStruct.NewPhoneStaticData, forKey: PublicStruct.PhoneStaticDataKey)
        
        TableSectionStruct.data[2]=PublicStruct.NewPhoneStaticData
        TableSectionStruct.phoneNumber = PublicStruct.NewPhoneStaticData
    }
    
    static func setDate (dbController: databaseController?){
        
        /////////////////////////////////
        //      set data for registration date
        /////////////////////////////////
        
        let date = NSDate()
        let calendar = NSCalendar.current
        let year = calendar.component(.year, from: date as Date)
        let month = calendar.component(.month, from: date as Date)
        let day = calendar.component(.day, from: date as Date)
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        
        let currentDate = "\(day)/\(month)/\(year) \(hour):\(minutes)"
        
        arraySetupForSetDate(currentDate: currentDate)
        dbController?.updateDate(date: currentDate)
    }
    
    static func arraySetupForSetDate (currentDate: String){
        PublicStruct.OldDate = PublicStruct.PersistentData.stringArray(forKey: PublicStruct.DateKey) ?? [String]() //get nsuser data to array
        PublicStruct.NewDate = PublicStruct.OldDate //get previous nsuserdefault data
        if currentDate != "nil" {
            PublicStruct.NewDate.append(currentDate) //append the new data to the old array
        }
        PublicStruct.PersistentData.set(PublicStruct.NewDate, forKey: PublicStruct.DateKey)
    }
}
