//
//  MapStruct.swift
//  ProjectTest
//
//  Created by Jeriko on 12/28/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit
import MapKit

struct MapStruct {
    let minLat: CGFloat = -6.45
    let maxLat: CGFloat = -6.14
    let minLon: CGFloat = 106.67
    let maxLon: CGFloat = 106.91
    static var initLocation = [CLLocation]()
    static var userHomes = [CLLocationCoordinate2D]()
    static var mapCenter = CLLocation(latitude: -6.21462, longitude: 106.84513)
    static var homesName = [String]()
    static var indexPath = Int()
    
    
    func randomCGFloat(min: CGFloat, max: CGFloat)->CGFloat{
        //        let final = CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (max - min) + min
        let final = CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(max - min) + min
        return final
    }
    
    func homesAppender (latitude: CGFloat, longitude: CGFloat){
        let tempLoc = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        MapStruct.userHomes.append(tempLoc)
    }
    
    func homesPopulator (latitude: [CGFloat], longitude: [CGFloat], count: Int){
        MapStruct.userHomes.removeAll()
        if count >= 0 {
            for i in 0...count {
                let tempLoc = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude[i]), longitude: CLLocationDegrees(longitude[i]))
                MapStruct.userHomes.append(tempLoc)
            }
        }
    }
}
