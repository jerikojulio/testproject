//
//  LoginButtonState.swift
//  ProjectTest
//
//  Created by Jeriko on 12/28/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import Foundation
import UIKit

extension UITextFieldDelegate {
    
    func buttonEnabler (){
        if PublicStruct.emailFieldFilled == true && PublicStruct.phoneFieldFilled == true {
            EnableButton()
        } else {
            DisableButton()
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stateupdated"), object: self)
        
    }
    
    func EnableButton(){
        TextFieldStruct.buttonState = true
        TextFieldStruct.buttonAlpha = 1
    }
    
    func DisableButton(){
        TextFieldStruct.buttonState = false
        TextFieldStruct.buttonAlpha = 0.1
    }
}
