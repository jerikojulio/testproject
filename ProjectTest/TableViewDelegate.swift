//
//  TableViewDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 12/23/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import MapKit
import Contacts
import ContactsUI

extension TableSectionViewController: UITableViewDelegate {
    
    /////////////////////////////////
    //headers
    /////////////////////////////////
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionHeader = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as! TableViewSection
        
        let headerTwo = tableView.dequeueReusableCell(withIdentifier: "sectioncell")
        let sectionHeaderTwo = headerTwo as! SecondSectionView
        
        switch section{
        case 0:
            sectionHeader.contentView.backgroundColor = UIColor(red: 122/255, green: 165/255, blue: 211/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "members.png")
        case 1:
            sectionHeader.contentView.backgroundColor = UIColor(red: 110/255, green: 187/255, blue: 191/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "email.png")
        case 2:
            sectionHeader.contentView.backgroundColor = UIColor(red: 122/255, green: 110/255, blue: 211/255, alpha: 1)
            sectionHeader.headerImage.image = #imageLiteral(resourceName: "phone.png")
            
        default:
            sectionHeader.backgroundColor = UIColor.clear
        }
        
        sectionHeader.sectionTitle.text = TableSectionStruct.SectionTitle[section]
        
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self,action: #selector(tapHeader))
        sectionHeader.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(tapHeader)))
        
        
        TableSectionStruct.currentSection = section
        sectionHeader.currentSection = section
        
        sectionHeader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let containerView = UIView(frame: sectionHeader.frame)
        containerView.addSubview(sectionHeader)
        
        if TableSectionStruct.HeaderType == 0 {
            return containerView
        } else {
            return sectionHeaderTwo.contentView
        }
        
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? TableViewSection else {
            print("tap failed")
            return
        }
        
        switch cell.currentSection {
        case 1:
            if TableSectionStruct.isSectionOpened[cell.currentSection] == true {
                datasource.setDatabase(Data: self.itemsEmpty)
                tableSectionView.reloadData()
                TableSectionStruct.isSectionOpened[cell.currentSection] = false
            } else {
                TableSectionStruct.data[cell.currentSection] = TableSectionStruct.dataBackup[cell.currentSection]
                datasource.setDatabase(Data: self.items)
                tableSectionView.reloadData()
                TableSectionStruct.isSectionOpened[cell.currentSection] = true
            }
            
        default:
            if TableSectionStruct.isSectionOpened[cell.currentSection] == true {
                TableSectionStruct.dataBackup[cell.currentSection] = TableSectionStruct.data[cell.currentSection]
                TableSectionStruct.mainTableView.beginUpdates()
                for i in (0..<TableSectionStruct.data[cell.currentSection].count).reversed() {
                    let currentIndex = NSIndexPath(row: i, section: cell.currentSection)
                    TableSectionStruct.mainTableView.deleteRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.automatic)
                    TableSectionStruct.data[cell.currentSection].remove(at: i)
                }
                TableSectionStruct.mainTableView.endUpdates()
                TableSectionStruct.isSectionOpened[cell.currentSection] = false
            } else {
                TableSectionStruct.data[cell.currentSection] = TableSectionStruct.dataBackup[cell.currentSection]
                TableSectionStruct.mainTableView.beginUpdates()
                for i in 0..<TableSectionStruct.data[cell.currentSection].count {
                    let currentIndex = NSIndexPath(row: i, section: cell.currentSection)
                    TableSectionStruct.mainTableView.insertRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.automatic)
                }
                TableSectionStruct.mainTableView.endUpdates()
                TableSectionStruct.isSectionOpened[cell.currentSection] = true
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3 {
            return 100
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath)
        
        switch indexPath.section{
            
        case 0:
            switch TableSectionStruct.currentCellType2[indexPath.row] {
            case .opened:
                TableSectionStruct.currentCellType2[indexPath.row] = .closed
                childCellRemove(indexPath: indexPath, tableView: tableView)
                if let arrowedCell = currentCell as? TableSectionViewCell{
                    arrowToRight(cell: arrowedCell, indexPath: indexPath.row)
                }
            case .closed:
                TableSectionStruct.currentCellType2[indexPath.row] = .opened
                childCellAdd(indexPath: indexPath, tableView: tableView)
                if let arrowedCell = currentCell as? TableSectionViewCell{
                    arrowToDown(cell: arrowedCell, indexPath: indexPath.row)
                }
            case .address:
                var NSUserDataLatitude = [CGFloat]()
                var NSUserDataLongitude = [CGFloat]()
                
                if let temp01 = PublicStruct.PersistentData.array(forKey: "randomLatitude") as? [CGFloat] {
                    NSUserDataLatitude = temp01
                }
                
                if let temp02 = PublicStruct.PersistentData.array(forKey: "randomLongitude") as? [CGFloat] {
                    NSUserDataLongitude = temp02
                }
                
                
                //            classes.tabBarController.selectedIndex = 1
                
                self.tabBarController?.selectedIndex = 1
                
                
                let indexForChild = ViewControllerTable.childRowChecker(indexrow: indexPath.row)
                let tempCenter = CLLocation(latitude: CLLocationDegrees(NSUserDataLatitude[indexPath.row - indexForChild]), longitude: CLLocationDegrees(NSUserDataLongitude[indexPath.row - indexForChild]))
                
                MapStruct.mapCenter = tempCenter
                MapStruct.indexPath = indexPath.row - indexForChild
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "centermapupdate"), object: self)
                
                PublicStruct.PersistentData.set(NSUserDataLatitude, forKey: "randomLatitude")
                PublicStruct.PersistentData.set(NSUserDataLongitude, forKey: "randomLongitude")
                
            case .phoneNumber:
                insertToContacts(index: indexPath.row)
            default:
                break
            }
            
        case 1:
            switch items[indexPath.row].cellState {
            case .opened:
                print(items[indexPath.row].cellState)
                items[indexPath.row].cellState = .closed
                
                var childIndex = indexPath
                childIndex.row = childIndex.row + 1
                
                tableView.beginUpdates()
                
                for i in (childIndex.row...childIndex.row + 3).reversed() {
                    
                    
                    let currentIndex = NSIndexPath(row: i, section: childIndex.section)
                    switch items[childIndex.row].cellState{
                    case .opened:
                        break
                    case .closed:
                        break
                    default:
                        tableView.deleteRows(at: [currentIndex as IndexPath], with: UITableViewRowAnimation.bottom)
                        items.remove(at: childIndex.row)
                        datasource.setDatabase(Data: self.items)
                    }
                }
                
                tableView.endUpdates()
                
            case .closed:
                print(items[indexPath.row].cellState)
                items[indexPath.row].cellState = .opened
                var currentData = items[indexPath.row]
                
                var childIndex = indexPath
                
                childIndex.row = childIndex.row + 1
                
                tableView.beginUpdates()
                
                var tempIndex = 1
                
                for i in childIndex.row...childIndex.row + 3{
                    
                    childIndex.row = i
                    
                    switch tempIndex {
                    case 1:
                        currentData.cellState = .nameCell
                    case 2:
                        currentData.cellState = .dateCell
                    case 3:
                        currentData.cellState = .address
                    case 4:
                        currentData.cellState = .phoneNumber
                    default:
                        break
                    }
                    
                    items.insert(currentData, at: i)
                    tableView.insertRows(at: [childIndex], with: UITableViewRowAnimation.top)
                    datasource.setDatabase(Data: self.items)
                    
                    tempIndex = tempIndex + 1
                }
                
                tableView.endUpdates()
                
                
            default:
                break
            }
            
            
        case 2:
            insertToContacts(index: indexPath.row)
            
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    /*
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     
     if TableSectionStruct.currentCellType2[indexPath.row] == .closed && indexPath.section == 0{
     if editingStyle == UITableViewCellEditingStyle.delete{
     
     TableSectionStruct.currentCellType2.remove(at: indexPath.row)
     PublicStruct.NewEmailStaticData.remove(at: indexPath.row)
     TableSectionStruct.data[0].remove(at: indexPath.row)
     TableSectionStruct.data[1].remove(at: indexPath.row)
     TableSectionStruct.data[2].remove(at: indexPath.row)
     tableView.reloadData()
     
     /////////////////////////////////
     //set data for members
     /////////////////////////////////
     PublicStruct.NewMemberDataIndex = PublicStruct.PersistentData.integer(forKey: PublicStruct.MemberDataIndexKey)
     
     PublicStruct.NewMemberDataIndex = PublicStruct.NewMemberDataIndex - 1
     
     PublicStruct.PersistentData.set(PublicStruct.NewMemberDataIndex, forKey: PublicStruct.MemberDataIndexKey)
     
     for i in 0...(PublicStruct.NewMemberDataIndex-1){
     TableSectionStruct.data[0][i] = String(i+1)
     }
     
     PublicStruct.NewMemberStaticData = TableSectionStruct.data[0].map{Int($0)!}
     
     PublicStruct.PersistentData.set(PublicStruct.NewMemberStaticData, forKey: PublicStruct.MemberStaticDataKey) // set the index before index is updated
     
     
     /////////////////////////////////
     //set data for email sections
     /////////////////////////////////
     PublicStruct.PersistentData.set(TableSectionStruct.data[1], forKey: PublicStruct.EmailStaticDataKey)
     
     /////////////////////////////////
     //set data for phone sections
     /////////////////////////////////
     PublicStruct.PersistentData.set(TableSectionStruct.data[2], forKey: PublicStruct.PhoneStaticDataKey)
     
     /////////////////////////////////
     //set data for registration date
     /////////////////////////////////
     PublicStruct.NewDate.remove(at: indexPath.row)
     
     PublicStruct.PersistentData.set(PublicStruct.NewDate, forKey: PublicStruct.DateKey)
     /////////////////////////////////
     //set data end
     /////////////////////////////////
     }
     }
     if items[indexPath.row].cellState == .closed && indexPath.section == 1 && editingStyle == .delete{
     let trash = items[indexPath.row]
     trash.ref?.removeValue()
     // delete the table view row
     tableView.deleteRows(at: [indexPath], with: .fade)
     tableView.reloadData()
     }
     
     }
     */
    
    
}

