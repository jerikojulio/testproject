//
//  FirebaseTableDelegate.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

extension TimerDate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
