//
//  Helper.swift
//  ProjectTest
//
//  Created by Jeriko on 12/21/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit
import Foundation

class Helper {
    static func gradientTopColor()-> CGColor{
//        return UIColor(red: 200.0/255.0, green: 68.0/255.0, blue: 52.0/255.0, alpha: 1.0).cgColor
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
    }
    
    static func gradientBottomColor() -> CGColor{
//        return UIColor(red: 85.0/255.0, green: 12.0/255.0, blue: 12.0/255.0, alpha: 1.0).cgColor
        return UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0).cgColor
    }
}
