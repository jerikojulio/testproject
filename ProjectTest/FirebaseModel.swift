//
//  FirebaseModel.swift
//  ProjectTest
//
//  Created by Jeriko on 1/18/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseDataModel {
    let name: String
    let ref: FIRDatabaseReference?
    
    init(name: String) {
        self.name = name
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        name = snapshotValue["name"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
        ]
    }
    
}
