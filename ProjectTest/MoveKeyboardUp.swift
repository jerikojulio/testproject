//
//  MoveKeyboardUp.swift
//  ProjectTest
//
//  Created by Jeriko on 12/26/16.
//  Copyright © 2016 Jeriko. All rights reserved.
//

import UIKit

extension MainPageViewController {
    
    func moveKeyboardUp(keyboardSize: CGRect, frameYaxis: CGFloat, textFieldHeight: CGFloat, responder: Bool){
        if self.view.frame.origin.y != 0 && responder
        {
            self.view.frame.origin.y = 0
        }
        
        if self.view.frame.origin.y == 0{
            if (frameYaxis + textFieldHeight) > (self.view.frame.height - keyboardSize.height)
            {self.view.frame.origin.y = self.view.frame.origin.y - keyboardSize.height}
        }
    }
    
}
